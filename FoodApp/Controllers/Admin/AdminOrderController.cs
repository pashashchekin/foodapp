﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FoodApp.Core.Models;
using FoodApp.DAL;
using FoodApp.DAL.DbContext;
using Microsoft.AspNetCore.Mvc;

namespace FoodApp.Controllers.Admin
{
    public class AdminOrderController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public AdminOrderController(ApplicationDbContext context, IUnitOfWorkFactory unitOfWorkFactory)
        {
            if (context == null)
                throw new ArgumentNullException(nameof(context));
            if (unitOfWorkFactory == null)
                throw new ArgumentNullException(nameof(unitOfWorkFactory));

            _context = context;
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public async Task<IActionResult> Index()
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var orders = unitOfWork.Order.GetAll();
                return View(orders);
            }
        }

        public async Task<IActionResult> Details(int? id)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                if (id == null) return NotFound();

                var order = unitOfWork.Order.GetById(id.Value);
                if (order == null) return NotFound();

                return View(order);
            }
        }
    }
}