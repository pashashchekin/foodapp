﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using FoodApp.Core.Models;
using FoodApp.DAL;
using FoodApp.DAL.DbContext;
using FoodApp.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace FoodApp.Controllers.Admin
{
    public class AdminDishController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public AdminDishController(ApplicationDbContext context, IUnitOfWorkFactory unitOfWorkFactory)
        {
            if (context == null)
                throw new ArgumentNullException(nameof(context));
            if (unitOfWorkFactory == null)
                throw new ArgumentNullException(nameof(unitOfWorkFactory));

            _context = context;
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        // GET: AdminDish
        public async Task<IActionResult> Index()
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var dishes = unitOfWork.Dish.GetAll();
                return View(dishes);
            }
        }

        // GET: AdminDish/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                if (id == null) return NotFound();

                var dish = unitOfWork.Dish.GetById(id.Value);
                if (dish == null) return NotFound();

                return View(dish);
            }
        }

        // GET: AdminDish/Create
        public IActionResult Create()
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var model = new CreateDishModel
                {
                    CafesSelectList = new SelectList(unitOfWork.Cafe.GetAll(), "Id", "Name")
                };
                return View(model);
            }
        }

        // POST: AdminDish/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(CreateDishModel model)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                if (ModelState.IsValid)
                {
                    var dish = Mapper.Map<Dish>(model);

                    await unitOfWork.Dish.AddAsync(dish);
                    await unitOfWork.CompleteAsync();

                    return RedirectToAction(nameof(Index));
                }

                model.CafesSelectList = new SelectList(unitOfWork.Cafe.GetAll(), "Id", "Name", model.CafeId);
                return View(model);
            }
        }

        // GET: AdminDish/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                if (!id.HasValue) return NotFound();

                var dish = unitOfWork.Dish.GetById(id.Value);

                var model = Mapper.Map<EditDishModel>(dish);

                model.CafesSelectList = new SelectList(unitOfWork.Cafe.GetAll(), "Id", "Name", model.CafeId);

                return View(model);
            }
        }

        // POST: AdminDish/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(EditDishModel model)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                if (ModelState.IsValid)
                {
                    var dish = Mapper.Map<Dish>(model);

                    unitOfWork.Dish.Update(dish);
                    await unitOfWork.CompleteAsync();

                    return RedirectToAction(nameof(Index));
                }

                model.CafesSelectList = new SelectList(unitOfWork.Cafe.GetAll(), "Id", "Name", model.CafeId);
                return View(model);
            }
        }

        // GET: AdminDish/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                if (id == null) return NotFound();

                var dish = unitOfWork.Dish.GetById(id.Value);
                if (dish == null) return NotFound();

                return View(dish);
            }
        }

        // POST: AdminDish/Delete/5
        [HttpPost]
        [ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Dish dish)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                unitOfWork.Dish.Delete(dish);
                await unitOfWork.CompleteAsync();
                return RedirectToAction(nameof(Index));
            }
        }

        private bool DishExists(int id)
        {
            return _context.Dishes.Any(e => e.Id == id);
        }
    }
}