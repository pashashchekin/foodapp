﻿using System;
using System.Linq;
using System.Threading.Tasks;
using FoodApp.Core.Models;
using FoodApp.DAL;
using FoodApp.DAL.DbContext;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace FoodApp.Controllers.Admin
{
    public class AdminCafeController : Controller
    {
        private readonly ApplicationDbContext _context;

        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public AdminCafeController(ApplicationDbContext context, IUnitOfWorkFactory unitOfWorkFactory)
        {
            if (context == null)
                throw new ArgumentNullException(nameof(context));
            if (unitOfWorkFactory == null)
                throw new ArgumentNullException(nameof(unitOfWorkFactory));

            _context = context;
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        // GET: AdminCafe
        public async Task<IActionResult> Index()
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var cafes = unitOfWork.Cafe.GetAll();
                return View(cafes);
            }
        }

        // GET: AdminCafe/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                if (!id.HasValue) return NotFound();

                var cafe = unitOfWork.Cafe.GetById(id.Value);
                if (cafe == null) return NotFound();

                return View(cafe);
            }
        }

        // GET: AdminCafe/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: AdminCafe/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Cafe cafe)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
                if (ModelState.IsValid)
                {
                    await unitOfWork.Cafe.AddAsync(cafe);
                    await unitOfWork.CompleteAsync();
                    return RedirectToAction(nameof(Index));
                }
            return View(cafe);
        }
    

        // GET: AdminCafe/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                if (!id.HasValue)
                {
                    return NotFound();
                }

                var cafe = unitOfWork.Cafe.GetById(id.Value);

                return View(cafe);
            }
        }

        // POST: AdminCafe/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Cafe cafe)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                if (ModelState.IsValid)
                {
                    try
                    {
                        unitOfWork.Cafe.Update(cafe);
                        await unitOfWork.CompleteAsync();
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                        if (!CafeExists(cafe.Id))
                            return NotFound();
                        throw;
                    }

                    return RedirectToAction(nameof(Index));
                }

                return View(cafe);
            }
        }

        // GET: AdminCafe/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                if (id == null) return NotFound();

                var cafe = unitOfWork.Cafe.GetById(id.Value);
                if (cafe == null) return NotFound();

                return View(cafe);
            }
        }

        // POST: AdminCafe/Delete/5
        [HttpPost]
        [ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Cafe cafe)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork()) 
            {
                unitOfWork.Cafe.Delete(cafe);
                await unitOfWork.CompleteAsync();
                return RedirectToAction(nameof(Index));
            }
        }

        private bool CafeExists(int id)
        {
            return _context.Cafes.Any(e => e.Id == id);
        }
    }
}