﻿using System.Threading.Tasks;
using FoodApp.Core.Models;
using Microsoft.AspNetCore.Mvc;
using FoodApp.DAL;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Routing;

namespace FoodApp.Controllers
{
    [Authorize]
    public class CafeController : Controller
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public CafeController(IUnitOfWorkFactory unitOfWorkFactory)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public IActionResult Index()
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var cafes = unitOfWork.Cafe.GetAll();
                return View(cafes);
            }
        }

        public async Task<IActionResult> Details(int? id)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                if (!id.HasValue)
                {
                    return NotFound();
                }

                var cafe = unitOfWork.Cafe.GetById(id.Value);
                if (cafe == null)
                {
                    return NotFound();
                }

                var orders = unitOfWork.Order.GetAll();
                return View(cafe);
            }
        }

        [Authorize(Roles = "Client")]
        public async Task<IActionResult> Review(int? id)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork()) {

                if (!id.HasValue)
                {
                    return NotFound();
                }

                var cafe = unitOfWork.Cafe.GetById(id.Value);
                if (cafe == null)
                {
                    return NotFound();
                }


                var review = new Review
                {
                    Rating = 2,
                    Comment = "qweq",
                    Cafe = cafe,
                };
                await unitOfWork.Review.AddAsync(review);
                await unitOfWork.CompleteAsync();

                return RedirectToAction("Details", new RouteValueDictionary(review));
            }
        }



    }
}
