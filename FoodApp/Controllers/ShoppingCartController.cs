﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using FoodApp.Core.Models;
using FoodApp.DAL;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FoodApp.Controllers
{
    [Authorize(Roles = "Client")]
    public class ShoppingCartController : Controller
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public ShoppingCartController(IUnitOfWorkFactory unitOfWorkFactory, IHttpContextAccessor httpContextAccessor)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task<IActionResult> AddToCart(int id)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var userName = _httpContextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.Name);
                var cart = unitOfWork.ShoppingCart.GetByName(userName);
                var dish = unitOfWork.Dish.GetById(id);
                var cartItems = unitOfWork.CartItem.GetByCartId(cart.Id);
                CartItem line = unitOfWork.CartItem.GetById(id);
                if (line == null)
                {
                    line = new CartItem
                    {
                        Dish = dish,
                        Quanity = 1,

                    };
                }
                else
                {
                    line.Quanity += 1;
                }

                var total = 0;
                foreach (var item in cartItems)
                {
                    total += (item.Quanity * item.Dish.Price);
                }

                cart.SubtotalPrice = total;
                cart.CartItems.Add(line);
                unitOfWork.ShoppingCart.Update(cart);
                await unitOfWork.CompleteAsync();
                return RedirectToAction(nameof(Basket));
            }

        }

        public async Task<IActionResult> Basket()
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var userName = _httpContextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.Name);
                var cart = unitOfWork.ShoppingCart.GetByName(userName);
                var cartItems = unitOfWork.CartItem.GetByCartId(cart.Id);
                return View(cartItems);
            }
        }

        public async Task<IActionResult> CreateOrder(Order order)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                
                order.OrderTime = DateTime.Now;
                return View(order);
            }
        }

        public async Task<IActionResult> Checkout(Order order)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var userName = _httpContextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.Name);
                var cart = unitOfWork.ShoppingCart.GetByName(userName);
                var cartItems = unitOfWork.CartItem.GetByCartId(cart.Id);

                var total = 0;
                foreach (var item in cartItems)
                {
                    var orderDetail = new OrderDetail
                    {
                        Quanity = item.Quanity,
                        Dish = item.Dish,
                        Order = order
                    };
                    total += (item.Quanity * item.Dish.Price);
                    order.OrderDetails.Add(orderDetail);
                    await unitOfWork.OrderDetail.AddAsync(orderDetail);
                    await unitOfWork.CompleteAsync();
                    unitOfWork.CartItem.Delete(item);
                    await unitOfWork.CompleteAsync();
                }
                order.TotalOrderPrice = total;
                unitOfWork.Order.Update(order);
                await unitOfWork.CompleteAsync();
                return RedirectToAction("ConfirmOrder");
            }
        }

        public IActionResult ConfirmOrder()
        {
            return View("ConfirmOrder");
        }
    }
}