﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using FoodApp.Core.Models;
using FoodApp.ViewModels;

namespace FoodApp
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            DishMapping();
        }

        private void DishMapping()
        {
            CreateMap<CreateDishModel, Dish>();
            CreateMap<EditDishModel, Dish>();
            CreateMap<Dish, EditDishModel>();
        }
    }
}
