﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using FoodApp.Core.Models;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace FoodApp.ViewModels
{
    public class EditDishModel
    {
        [Required]
        public int Id { get; set; }

        [Display(Name = "Название")]
        public string Name { get; set; }

        [Display(Name = "Цена")]
        public string Price { get; set; }

        [Display(Name = "Описание")]
        public string Description { get; set; }

        [Required]
        [Display(Name = "Кафе")]
        public int CafeId { get; set; }

        public SelectList CafesSelectList { get; set; }
    }
}
