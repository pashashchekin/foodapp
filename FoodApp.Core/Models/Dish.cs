﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FoodApp.Core.Models
{
    public class Dish : Entity
    {
        public string Name { get; set; }
        public int Price { get; set; }
        public string Description { get; set; }
        public int CafeId { get; set; }
        public Cafe Cafe { get; set; }
    }
}
