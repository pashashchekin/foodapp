﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace FoodApp.Core.Models
{
    public class Cafe : Entity
    {
        public string Name { get; set; }
        public string Picture { get; set; }
        public string Description { get; set; }
        public ICollection<Dish> Dishes { get; set; }
        public ICollection<Review> Reviews { get; set; }

        public Cafe()
        {
            Dishes = new List<Dish>();
            Reviews = new List<Review>();
        }


    }
}
