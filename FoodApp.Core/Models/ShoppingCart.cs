﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Net.Http.Headers;
using System.Text;

namespace FoodApp.Core.Models
{
    public class ShoppingCart : Entity
    {
        public int SubtotalPrice { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
        public List<CartItem> CartItems { get; set; }
        public ShoppingCart()
        {
            CartItems = new List<CartItem>();
        }

        public int GetTotalPrice()
        {
            int total = 0;
            foreach (var cartItem in CartItems )
            {
                total += (cartItem.Quanity * cartItem.Dish.Price);
            }

            return total;
        }

    }
}
