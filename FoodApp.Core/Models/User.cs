﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity;

namespace FoodApp.Core.Models
{
    public class User : IdentityUser<int>
    {
        public User()
        {
            Reviews = new List<Review>();
        }
        public ICollection<Review> Reviews { get; set; }
        public ShoppingCart ShoppingCart { get; set; } = new ShoppingCart();
        
    }
}
