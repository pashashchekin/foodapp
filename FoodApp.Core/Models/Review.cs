﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FoodApp.Core.Models
{
    public class Review : Entity
    {

        public int Rating { get; set; }
        public string Comment { get; set; }
        public int CafeId { get; set; }
        public Cafe Cafe { get; set; }
        public int UserId { get; set; }
        public User User;


        public Review()
        {
            
        }
    }
}
