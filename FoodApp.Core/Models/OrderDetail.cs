﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FoodApp.Core.Models
{
    public class OrderDetail : Entity
    {
        public int Quanity { get; set; }
        public Dish Dish { get; set; }
        public int DishId { get; set; }
        public Order Order { get; set; }
    }
}
