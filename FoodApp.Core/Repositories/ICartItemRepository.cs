﻿using System;
using System.Collections.Generic;
using System.Text;
using FoodApp.Core.Models;

namespace FoodApp.Core.Repositories
{
    public interface ICartItemRepository : IRepository<CartItem>
    {
        ICollection<CartItem> GetByCartId(int id );
    }
}
