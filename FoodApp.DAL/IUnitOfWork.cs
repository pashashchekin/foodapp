﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using FoodApp.Core.Models;
using FoodApp.Core.Repositories;

namespace FoodApp.DAL
{
    public interface IUnitOfWork : IDisposable
    {
        ICafeRepository Cafe { get; }
        IDishRepository Dish { get; }
        IShoppingCartRepository ShoppingCart { get; }
        ICartItemRepository CartItem { get; }
        IReviewRepository Review { get; }
        IOrderRepository Order { get; }
        IOrderDetailRepository OrderDetail { get; }

        Task<int> CompleteAsync();
        void BeginTransaction();
        void RollbackTransaction();
        void CommitTransaction();
        IRepository<TEntity> GetRepository<TEntity>() where TEntity : Entity;
    }
}
