﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FoodApp.Core.Models;
using FoodApp.Core.Repositories;
using FoodApp.DAL.DbContext;
using Microsoft.EntityFrameworkCore;

namespace FoodApp.DAL.Repositories
{
    public class CafeRepository : Repository<Cafe>, ICafeRepository
    {
        public CafeRepository(ApplicationDbContext context) : base(context)
        {
            DbSet = context.Cafes;
        }

        public override Cafe GetById(int id)
        {
            return DbSet
                .Include(p => p.Dishes)
                .FirstOrDefault(p => p.Id == id);
        }
    }
}
