﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FoodApp.Core.Models;
using FoodApp.Core.Repositories;
using FoodApp.DAL.DbContext;
using Microsoft.EntityFrameworkCore;

namespace FoodApp.DAL.Repositories
{
    class ShoppingCartRepository : Repository<ShoppingCart>, IShoppingCartRepository
    {
        public ShoppingCartRepository(ApplicationDbContext context) : base(context)
        {
            DbSet = context.ShoppingCarts;
        }

        public override IEnumerable<ShoppingCart> GetAll()
        {
            return DbSet.Include(p => p.CartItems).ToList();
        }

        public ShoppingCart GetByName(string modelName)
        {
            return DbSet
                .Include(p => p.CartItems)
                .Include(p => p.CartItems)
                .FirstOrDefault(p => p.User.UserName == modelName);
        }
    }
}
