﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FoodApp.Core.Models;
using FoodApp.Core.Repositories;
using FoodApp.DAL.DbContext;
using Microsoft.EntityFrameworkCore;

namespace FoodApp.DAL.Repositories
{
    public class OrderRepository : Repository<Order>, IOrderRepository
    {
        public OrderRepository(ApplicationDbContext context) : base(context)
        {
            DbSet = context.Orders;
        }

        public override IEnumerable<Order> GetAll()
        {
            return DbSet.Include(p => p.OrderDetails).ToList();
        }

        public override Order GetById(int id)
        {
            return DbSet.Include(q => q.OrderDetails).ThenInclude(e => e.Dish.Cafe)
                .FirstOrDefault(q => q.Id == id);
        }
    }
}
