﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FoodApp.Core.Models;
using FoodApp.Core.Repositories;
using FoodApp.DAL.DbContext;
using Microsoft.EntityFrameworkCore;

namespace FoodApp.DAL.Repositories
{
    public class OrderDetailRepository : Repository<OrderDetail>, IOrderDetailRepository
    {
        public OrderDetailRepository(ApplicationDbContext context) : base(context)
        {
            DbSet = context.OrderDetails;
        }

        public override IEnumerable<OrderDetail> GetAll()
        {
            return DbSet
                .Include(p => p.Dish)
                .Include(p => p.Order);
        }

     
    }
}
