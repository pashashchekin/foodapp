﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FoodApp.Core.Models;
using FoodApp.Core.Repositories;
using FoodApp.DAL.DbContext;
using Microsoft.EntityFrameworkCore;

namespace FoodApp.DAL.Repositories
{
    public class CartItemRepository : Repository<CartItem>, ICartItemRepository
    {
        public CartItemRepository(ApplicationDbContext context) : base(context)
        {
            DbSet = context.CartItems;
        }

        public ICollection<CartItem> GetByCartId(int id)
        {
            return DbSet.Include(p => p.Dish).ToList();
        }

        public override CartItem GetById(int id)
        {
            return DbSet
                    .Include(p => p.Dish)
                    .FirstOrDefault(p => p.DishId == id);
        }
    }
}