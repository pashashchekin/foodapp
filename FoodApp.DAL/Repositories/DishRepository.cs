﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FoodApp.Core.Models;
using FoodApp.Core.Repositories;
using FoodApp.DAL.DbContext;
using Microsoft.EntityFrameworkCore;

namespace FoodApp.DAL.Repositories
{
    public class DishRepository : Repository<Dish>, IDishRepository
    {
        public DishRepository(ApplicationDbContext context) : base(context)
        {
            DbSet = context.Dishes;
        }

        public override IEnumerable<Dish> GetAll()
        {
            return DbSet.Include(p => p.Cafe).ToList();
        }

        public override Dish GetById(int id)
        {
            return DbSet
                .Include(p => p.Cafe)
                .FirstOrDefault(p => p.Id == id);
        }
    }
}