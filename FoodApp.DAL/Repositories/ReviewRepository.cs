﻿using System;
using System.Collections.Generic;
using System.Text;
using FoodApp.Core.Models;
using FoodApp.Core.Repositories;
using FoodApp.DAL.DbContext;

namespace FoodApp.DAL.Repositories
{
    public class ReviewRepository: Repository<Review>, IReviewRepository
    {
        public ReviewRepository(ApplicationDbContext context) : base(context)
        {
            DbSet = context.Reviews;
        }
    }
}
