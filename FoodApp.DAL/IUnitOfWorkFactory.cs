﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FoodApp.DAL
{
    public interface IUnitOfWorkFactory
    {
        IUnitOfWork MakeUnitOfWork();
    }
}
