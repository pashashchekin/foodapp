﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FoodApp.DAL.DbContext.Contracts
{
    public interface IApplicationDbContextFactory
    {
        ApplicationDbContext Create();
    }
}
