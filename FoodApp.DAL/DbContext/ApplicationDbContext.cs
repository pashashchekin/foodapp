﻿using System;
using System.Collections.Generic;
using System.Text;
using FoodApp.Core.Models;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace FoodApp.DAL.DbContext
{
    public class ApplicationDbContext : IdentityDbContext<User, Role, int>
    {
        public DbSet<Cafe> Cafes { get; set; }
        public DbSet<Dish> Dishes { get; set; }
        public DbSet<ShoppingCart> ShoppingCarts { get; set; }
        public DbSet<CartItem> CartItems { get; set; }
        public DbSet<Review> Reviews { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderDetail> OrderDetails { get; set; }


        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
    }
}
